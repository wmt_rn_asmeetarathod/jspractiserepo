var programming ={
    languages:["JavaScript","Python","Ruby"],
    isChallenging: true,
    isRewarding:true,
    difficulty:8,
    jokes:"http://stackoverflow.com/questions/234075/what-is-your-best-programmer-joke"
};

//1
programming['languages'][programming['languages'].length]="Go";

//2
programming['difficulty']=7;

//3
function del(){
    delete programming['jokes'];
}

//4
programming['isFun']=true;

console.log("length==>> ",Object.keys(programming).length);

//5
for(let j of programming['languages']){
    console.log("Language ==>> ",j);
}


//6
for(let i in programming){
    console.log("key ==>> ",i);
}

//7
for(let i in programming){
    console.log("Value ==>> ",programming[i]);
}

console.log("Values : ",Object.entries(programming));
del();
console.log("After Delete Jokes Key ");
console.log("Values : ",Object.entries(programming));