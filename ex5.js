
function getFrequent(arr){
	
	let z=0;
	var obj=[];
	for(let i of arr){
		let count=0;

		for(let j=0;j<arr.length;j++)
		{
			if(i==arr[j]){
				count++;
			}
		}
		obj[z]={
			val:i,
			counter:count
		};
		z++;
	}
	let maxCount=obj.sort(function(a,b){return b.counter-a.counter});
	return maxCount[0];

}

let element=getFrequent([3,'a','a','a',2,3,'a',3,'a',2,4,9,3]);

console.log("Most Frequent Element: ",element.val,"(",element.counter,"times)");


