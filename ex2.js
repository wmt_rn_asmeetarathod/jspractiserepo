
function last(arr,n=0)
{
	let ele=[];
	if(n>arr.length){
		ele=arr.slice(0);
	}
	else if(n==0)
	{
		ele=arr[arr.length-1];
	}
	else
	{
		ele=arr.slice(arr.length-n);
	}
	return ele;
}

console.log("op1 : "+last([7,9,0,-2]));
console.log("op2 : "+last([7,9,0,-2],3));
console.log("op3 : "+last([7,9,0,-2],6));


