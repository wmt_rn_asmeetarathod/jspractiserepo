"use strict";
const a=8;

function getNumber(resolve,reject){
    try{
        setTimeout(
            function () {
                const randomInt=Date.now();
                const value=randomInt%10;
                try {
                    if(value>=a){
                        throw new Error(`Too Large ${value}`);
                    }
                } catch (msg) {
                    reject(`Error in callback ${msg}`);
                }
                resolve(value);
                return ;
            },500);
    }catch(err){
        reject(`Error during setup : ${err}`);
    }
    return;
}

function determineParity(value) {
    const isOdd= value%2 ? true : false;
    const parityInfo={theNumber :value,isOdd : isOdd};
    return parityInfo;
}

function troubleWithGetNum(reason){
    console.error(`Trouble getting Number : ${reason}`);
    throw -999;
}

function promiseGetWord(parityInfo) {
    let tetherWord=function(resolve,reject){
        const theNumber=parityInfo.theNumber;
        const b=a-1;
        if(theNumber>=b){
            reject(`Still too Large : ${theNumber}`);
        }else{
            parityInfo.wordEvenOdd=parityInfo.isOdd ? 'odd' :'even';
            resolve(parityInfo);
        }
        return;
    }
    return new Promise(tetherWord);
}


(new Promise(getNumber))
.then(determineParity,troubleWithGetNum)
.then(promiseGetWord)
.then((info)=> {
    console.log("Got : ",info.theNumber,",",info.wordEvenOdd)
    return info;
}).catch((reason)=> {
    if(reason === -999){
        console.error("Has previously handled error");
    }
    else{
        console.error(`Trouble with promiseGetWord() : ${reason}`);
    }
})
.finally((info)=> console.log("All Done!"));
