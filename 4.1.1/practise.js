const key ='abc';
let points=50;
let winner =false;

//let points=60;

if(points>40){
    winner =true;
    console.log("winner in if ==>> ",winner);
}
console.log("winner  ==>> ",0=="false");

console.log("includes Example ==>> ",'apple'.includes('el'));
console.log("repeat Example ==>> ",'as'.repeat(3));


let [a,b]=[3,4];
console.log("a ==>> ",a);
console.log("b ==>> ",b);


//Spread Opearator
const x=[1,2];
const y=[3,4];

const z=[...x,...y];
console.log("z ==>> ",z);

//End Spread Opearator


//let scope
let as=4;

if(true){
    as=5;
    console.log("as in IF ==>> ",as);
}

console.log("as ==>> ",as);

//End let scope

//template String

const name="Asmeeta";
const nm=`Hello ${name}`;

console.log("nm ==>> ",nm);

function func1(){
    let temp=4;
}
//console.log("temp Value ==>> ",temp);

// template literal

let cus={name:"Asmeeta"};
let card={amount : 4,product: "Phone", unitprice : 40};
let message=` Hello ${cus.name} want to buy ${card.amount} ${card.product} for a total of ${card.amount*card.unitprice} !!`;
console.log("Message ==>> ",message);


function func1(str,...values){
    console.log("str ==>> ",str);
    console.log("values ==>> ",values);
    str[0]==="foo\n";
    str[1]==="bar";
    str.raw[0]==="foo\\n";
    str.raw[1]==="bar";
    values[0]===42;
}

func1 `foo\n${42}bar`
console.log(String.raw`foo\n${42}bar`===`foo\\n${42}bar`);

console.log("foo\\n42bar");


console.log("binary ==>> ",0b0101==5);

console.log("octal ==>> ",0o767===503);

console.log('𠮷.length === 2',"𠮷".length === 2);

let dest={qux:0};
let obj1={name: "asmeeta"};
let obj2={foo : 1,bar : 2};
let obj3={foo:3,bar :4};
Object.assign(dest,obj1,obj2,obj3);

console.log("dest ==>> ",dest);

console.log("repeat ==>> ","as".repeat(4));

console.log("isNaN() ==>> ",Number.isNaN(41));
console.log("isFinite() ==>> ",Number.isFinite(Infinity));
console.log("isSafeInteger() ==>> ",Number.isSafeInteger(80002303679238741));
console.log("(0.1+0.2)==0.3 ==>>",(0.1+0.2)==0.3);

console.log(Math.abs((0.1+0.2)-0.3)< Number.EPSILON);

console.log("trunc ==> ",Math.trunc(4.3349857));

console.log("Math.sign ==>> ",Math.sign(NaN));