let arr=[
  {
    "_id": "5c484382ec1c46ca4c29db9c",
    "index": 0,
    "guid": "a04af7bd-ef4e-4730-bb9f-78834178b027",
    "isActive": true,
    "balance": "$1,533.69",
    "picture": "http://placehold.it/32x32",
    "age": 40,
    "eyeColor": "green",
    "name": "Mcpherson Lang"
  },
  {
    "_id": "5c484382b9ebb436fd9e1bd9",
    "index": 1,
    "guid": "7fa38b9c-9522-4518-8b5c-e443b4e60621",
    "isActive": false,
    "balance": "$2,595.37",
    "picture": "http://placehold.it/32x32",
    "age": 30,
    "eyeColor": "blue",
    "name": "Villarreal Wall"
  },
  {
    "_id": "5c4843823624873b4571ffeb",
    "index": 2,
    "guid": "4f9b63c0-dda7-4e52-859e-e4eb9ff542f4",
    "isActive": false,
    "balance": "$2,410.54",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "eyeColor": "blue",
    "name": "Kayla Calhoun"
  },
  {
    "_id": "5c484382ae74e041bfd2673a",
    "index": 3,
    "guid": "364f594a-9cf4-42f5-9570-c32cb889b6db",
    "isActive": false,
    "balance": "$1,483.55",
    "picture": "http://placehold.it/32x32",
    "age": 24,
    "eyeColor": "green",
    "name": "Warner Hurst"
  },
  {
    "_id": "5c48438266279bc15c3b1832",
    "index": 4,
    "guid": "2f657f75-5418-460c-856b-e5c4cf532320",
    "isActive": true,
    "balance": "$1,147.33",
    "picture": "http://placehold.it/32x32",
    "age": 30,
    "eyeColor": "green",
    "name": "Ruiz Mcneil"
  },
  {
    "_id": "5c48438274240a2910ccb9bd",
    "index": 5,
    "guid": "dc28098a-9195-45c0-a50b-fc18e3a7d65c",
    "isActive": false,
    "balance": "$1,060.17",
    "picture": "http://placehold.it/32x32",
    "age": 22,
    "eyeColor": "brown",
    "name": "Kristina Harvey"
  }
]

//1 ==>>
let arr1=arr;
console.log("Sorted by Name ==>> ",arr1.sort((a,b)=>{
    return a['name'].toString().localeCompare(b['name']);
}));

console.log("Sorted by Age ==>> ",arr1.sort((a,b)=>{
    return a['age'].toString().localeCompare(b['age']);
}));

console.log("Sorted by Balance ==>> ",arr1.sort((a,b)=>{
    return a['balance'].toString().localeCompare(b['balance']);
}));

// 1 completed

//2 ==>>
let obj4Arr=[];
for(let i of arr){
    let obj4={};
    obj4['_id']=i['_id'];
    obj4['index']=i['index'];
    obj4['guid']=i['guid'];
    obj4['name']=i['name'];
    obj4Arr.push(obj4);
    
}
obj4Arr.sort((a,b)=>{
    return a['index'].toString().localeCompare(b['index']);
})
console.log("obj4Arr ==>> ",obj4Arr);

//2 completed

// 3 ==>>

let isActiveArr=arr.filter((val,index,array)=>{
    return val['isActive']==false;
})
console.log("isActive False Filtered Array ==>> ",isActiveArr);

// 3 completed

//4 ==>>

let ageArr=arr.filter((val,index,array)=>{
    return val['age']<30;
})

console.log("Array of age < 30 ==> ",ageArr);

// 4 completed

// 5 ==>>

let eyeArr=arr.filter((val,index,array)=>{
    return val['eyeColor']=='green' || val['eyeColor']=='brown';
})

console.log("eyeColor (green|brown) Array ==>> ",eyeArr);

// 5 completed

// 6 ==>>
let arr2=arr;

function balanceSum() {
    let sum=0.0;
    for(let i of arr2){
        let s=i['balance'].replace("$","").replace(",","");
        sum +=parseFloat(s);
        console.log("balance ==>>>> ",s);
    }
    return sum;
}
console.log("Sum of Balance ==>> ",balanceSum());

// 6 completed

// 7 ==>>
let warnerArr=arr.filter((val,index)=>{
    return val['name']=='Warner Hurst';
})

console.log("Array (warner Hurst) ==>> ",warnerArr);

// 7 completed

// 8 ==>>

for(let i of arr){
    i['lottery_number']=Math.floor(Math.random() *100+1);
}

console.log("Key(lottery_number) added Array ==>> ",Object.entries(arr));

// 8 completed