let student={
    name:"David Rayy",
    sclass:"VI",
    rollno:12,
};
function del(){
    delete student.rollno;
}
//student.assign("Asmeeta","IT",41);
console.log("Before Delete : ");
console.log(Object.keys(student));
del();
console.log("After Delete : ");
console.log(Object.keys(student));