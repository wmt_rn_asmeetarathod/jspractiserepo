let student={
    name:"David Rayy",
    sclass:"VI",
    rollno:12,
};

let getKeys=Object.keys(student);
let getValues=Object.values(student);
console.log("keys==> ",getKeys);
console.log("values ==>",getValues);

let stuCopyArr=[];
for(let i in getKeys)
{   
    let stuCopy={};
    console.log("i : ",i);
    console.log("Value[i] : ",getValues[i]);
    console.log("Keys[i] : ",getKeys[i]);
    
    stuCopy[getValues[i]]=getKeys[i];
    stuCopyArr.push(stuCopy);
  
}

console.log("Values of Student Object ==>> ",Object.entries(student));

console.log("Swapped ==> ",stuCopyArr);


