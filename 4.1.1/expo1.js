let defaultExports={};


defaultExports.square=function (x){
    return x*x;
}

defaultExports.sum= function (x,y){
    return x+y;
}

defaultExports.myClass=class {
    mul(x,y){
        return x*y;
    }
}

Object.defineProperty(exports,"_esmodule",{
    value:true
});

exports["default"]=defaultExports;


